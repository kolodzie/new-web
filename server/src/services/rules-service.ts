import { Rule } from "../models/rule";

export interface RulesService {

  createRule(rule: Rule): Promise<Rule>;

}