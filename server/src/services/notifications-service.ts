import { Notification } from "../models/notification";

export interface NotificationsService {
  sendNotification(notification: Notification): Promise<Notification>;
  findAllNotifications(userId: number): Promise<Notification[]>;
}
