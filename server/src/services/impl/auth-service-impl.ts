import { AuthService } from "../auth-service";
import { CommandExecutor } from "./command-executor";
import { Login } from "./auth/login/Login";
import { RefreshTokens } from "./auth/refreshtokens/RefreshTokens";
import { AbstractService } from "./abstract-service";
import { User } from "../../models/user";
import { GetAuthenticatedUser } from "./auth/get-authenticated-user/GetAuthenticatedUser";

export class AuthServiceImpl extends AbstractService implements AuthService {
  authenticateUser(
    code: string,
    service: string
  ): Promise<{
    randomString: string;
    accessToken: string;
    refreshToken: string;
  }> {
    return this.commandExecutor.execute(new Login(code, service));
  }

  refreshTokens(
    tokenPayload
  ): Promise<{
    randomString: string;
    accessToken: string;
    refreshToken: string;
  }> {
    return this.commandExecutor.execute(new RefreshTokens(tokenPayload));
  }

  getAuthenticatedUser(userId: string): Promise<User> {
    return this.commandExecutor.execute(new GetAuthenticatedUser(userId))
  }
}
