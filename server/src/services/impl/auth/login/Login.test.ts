import { Login } from "./Login";
import { EntityManager } from "typeorm";
import { mock, instance } from "ts-mockito"
import { CernOauth } from "../../../../models/cern-oauth";

jest.mock("../../../../models/cern-oauth.ts")
jest.mock("../../../../models/cache.ts")


describe("Login test", () => {
  beforeAll((done) => {
    process.env.JWT_SECRET = "secret"
    done()
  })

  it("Should throw an error when the user does not exist", async () => {
    expect.assertions(1);
    try {
      const login = new Login("false", CernOauth.WEB_SERVICE) // This mocks the case in which the user code is not correct
      await login.execute(instance(mock(EntityManager)))
    } catch (e) {
      expect(e).toEqual(new Error("Error when trying to authenticate the user: Error: The user does not exist"))
    }
  })
  it("Should return new tokens when the user exists", async () => {
    expect.assertions(3);

    const login = new Login("true", "app") // This mocks the case in which the user code is correct

    const { accessToken, randomString, refreshToken } = await login.execute(instance(mock(EntityManager)))

    expect(accessToken).not.toBeInstanceOf(String)
    expect(randomString).not.toBeInstanceOf(String)
    expect(refreshToken).not.toBeInstanceOf(String)

  })
})