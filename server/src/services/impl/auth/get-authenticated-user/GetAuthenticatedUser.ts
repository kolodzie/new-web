import { Command } from "../../command";
import { EntityManager } from "typeorm";
import { User } from "../../../../models/user";

export class GetAuthenticatedUser implements Command{

    constructor(private userId: string){

    }

    async execute(transactionManager: EntityManager): Promise<User> {
        return await transactionManager.findOne(User, this.userId)
    }

}