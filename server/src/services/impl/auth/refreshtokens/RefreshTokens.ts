import { Command } from "../../command";
import { User } from "../../../../models/user";
import { EntityManager } from "typeorm";
import { Authentication } from "../../../../models/authentication";

export class RefreshTokens implements Command {
  constructor(private tokenPayload) { }

  async execute(
    transactionManager: EntityManager
  ): Promise<{
    randomString: string;
    accessToken: string;
    refreshToken: string;
  }> {
    const user = await transactionManager.findOneOrFail(User, this.tokenPayload.id);

    if (!Authentication.removeRefreshTokenFromWhiteList(this.tokenPayload))
      throw new Error("Error when trying to remove the refresh token from the white list")
    return Authentication.getTokens(user);
  }
}
