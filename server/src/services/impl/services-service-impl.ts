import { ServicesService } from "../services-service";
import { CommandExecutor } from "./command-executor";
import { CreateService } from "./services/create-service/create-service";
import { Service } from "../../models/service";
import { FindServiceByApiKey } from "./services/find-service-by-api-key/find-service-by-api-key";
import { AbstractService } from "./abstract-service";
import { FindAllServices } from "./services/find-all-services/find-all-services";

export class ServicesServiceImpl extends AbstractService implements ServicesService {

  createService(serviceName: string): Promise<string> {
    return this.commandExecutor.execute(new CreateService(serviceName))
  }

  findServiceByApiKey(apiKey: string): Promise<Service> {
    return this.commandExecutor.execute(new FindServiceByApiKey(apiKey))
  }

  findAllServices(): Promise<Service[]> {
    return this.commandExecutor.execute(new FindAllServices())
  }
}