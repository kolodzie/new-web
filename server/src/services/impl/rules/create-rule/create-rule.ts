import { Command } from "../../command";
import { EntityManager } from "typeorm";
import { Rule } from "../../../../models/rule";


export class CreateRule implements Command {
  constructor(private rule: Rule) { }

  execute(transactionManager: EntityManager): Promise<Rule> {
    return transactionManager.save(this.rule)
  }

}