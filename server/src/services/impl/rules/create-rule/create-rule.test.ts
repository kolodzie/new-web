import { TestUtils } from "../../../../utils/testUtils";
import { getManager } from "typeorm";
import { User } from "../../../../models/user";
import { AbstractRule } from "../../../../models/rules/abstractRule";
import { RulesServiceImpl } from "../../rules-service-impl";
import { AllByEmailOrPush } from "../../../../models/rules/allByEmailOrPush";
import { Channels } from "../../../../models/rules/ChannelsList";

jest.mock("../../../../models/cache.ts")

const userId = 1;

describe("create rule tests", () => {
  beforeAll(async () => {
    await TestUtils.createDatabaseConnection()
  })

  beforeEach(async (done) => {
    await getManager().transaction(async manager => {
      await manager.delete(User, {})
      await manager.save(new User({
        id: userId,
        email: "email@email.com",
        username: "username"
      }))
      await manager.delete(AbstractRule, {});
    })

    done()
  })

  it("Should create a new rule when the channel is correct", async () => {
    let rule = await new RulesServiceImpl().createRule(new AllByEmailOrPush({
      channel: Channels.email
    }))

    expect(rule).toBeInstanceOf(AbstractRule)

    rule = await new RulesServiceImpl().createRule(new AllByEmailOrPush({
      channel: Channels.push
    }))

    expect(rule).toBeInstanceOf(AbstractRule)
  })

  it("Should create a new rule when the channel is correct", async () => {
    let rule = await new RulesServiceImpl().createRule(new AllByEmailOrPush({
      channel: Channels.email
    }))

    expect(rule).toBeInstanceOf(AbstractRule)

    rule = await new RulesServiceImpl().createRule(new AllByEmailOrPush({
      channel: Channels.push
    }))

    expect(rule).toBeInstanceOf(AbstractRule)
  })
})