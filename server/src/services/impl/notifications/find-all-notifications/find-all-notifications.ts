import { Command } from "../../command";
import { EntityManager } from "typeorm";
import { User } from "../../../../models/user";

export class FindAllNotifications implements Command {

  constructor(private userId: number) { }

  async execute(transactionalManager: EntityManager) {
    const user = await transactionalManager.findOne(User, { where: { id: this.userId }, relations: ["notifications", "notifications.from"] });

    return user.notifications;
  }
}
