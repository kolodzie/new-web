import { NotificationsServiceImpl } from "../../notification-service-impl";
import { getManager, EntityManager } from "typeorm";
import { TestUtils } from "../../../../utils/testUtils";
import { Notification } from "../../../../models/notification";
import { User } from "../../../../models/user";

jest.mock("../../../../models/cache.ts")

const userId = 1;

describe("find all notifications test", () => {

  beforeAll(async (done) => {
    await TestUtils.createDatabaseConnection()
    done()
  })

  beforeEach(async (done) => {
    await getManager().transaction(async manager => {
      await manager.delete(User, {})
      await manager.delete("users_notifications__notifications", {})
      await manager.save(new User({
        id: userId,
        email: "email@email.com",
        username: "username"
      }))
      await manager.delete(Notification, {});
    })
    done()
  })

  it("should return no notifications when no notifications exist in the database", async () => {
    const notifications = await new NotificationsServiceImpl().findAllNotifications(userId)
    expect(notifications).toEqual([])
  })

  it("should return no notifications when no notification from the user exist in the database", async () => {
    await getManager().transaction(async manager => {
      const user = await manager.save(new User({
        id: 2,
        email: "email@email.com",
        username: "username",
        notifications: [new Notification({
          id: 1,
          from: "from",
          target: "target",
          subject: "subject",
          body: "body"
        })]
      }))
      await manager.save(user)
    })
    const notifications = await new NotificationsServiceImpl().findAllNotifications(userId)
    expect(notifications).toEqual([])
  })

  it("should return 1 notification when the user has received only 1", async () => {
    await getManager().transaction(async manager => {
      const user = await manager.findOne(User, { relations: ["notifications"] })
      user.notifications.push(await manager.save(new Notification({
        from: "from",
        target: "target",
        subject: "subject",
        body: "body"
      })))
      await manager.save(user)
    })

    const notifications = await new NotificationsServiceImpl().findAllNotifications(userId)
    expect(notifications.length).toBe(1)
  })

  it("should return 2 notifications when the user has received 2", async () => {
    await getManager().transaction(async manager => {
      const user = await manager.findOne(User, { relations: ["notifications"] })
      user.notifications.push(...await manager.save([new Notification({
        from: "from",
        target: "target",
        subject: "subject",
        body: "body"
      }),
      new Notification({
        from: "from",
        target: "target",
        subject: "subject",
        body: "body"
      })
      ]))
      await manager.save(user)
    })
    const notifications = await new NotificationsServiceImpl().findAllNotifications(userId)
    expect(notifications.length).toBe(2)
  })

})