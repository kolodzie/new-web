import { RulesService } from "../rules-service";
import { Rule } from "../../models/rule";
import { AbstractService } from "./abstract-service";
import { CreateRule } from "./rules/create-rule/create-rule";

export class RulesServiceImpl extends AbstractService implements RulesService {

  createRule(rule: Rule): Promise<Rule> {
    return this.commandExecutor.execute(new CreateRule(rule))
  }

}