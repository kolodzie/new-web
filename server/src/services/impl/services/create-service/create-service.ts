
import { Command } from '../../command';
import { EntityManager } from 'typeorm';
import { Service } from '../../../../models/service';

export class CreateService implements Command {

  constructor(private serviceName: string) { }

  async execute(transactionManager: EntityManager): Promise<string> {
    const service = new Service(this.serviceName)
    const apiKey = service.generateNewApiKey();
    await transactionManager.save(service)

    return apiKey
  }
}