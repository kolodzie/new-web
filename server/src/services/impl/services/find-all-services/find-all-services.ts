import { Command } from "../../command";
import { EntityManager } from "typeorm";
import { Service } from "../../../../models/service";


export class FindAllServices implements Command {

  constructor() { }

  execute(transactionManager: EntityManager): Promise<Service[]> {
    return transactionManager.find(Service)
  }

}