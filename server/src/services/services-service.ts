import { Service } from "../models/service";


export interface ServicesService {
  /**
   * 
   * @param {string} serviceName The name of the new service.
   * 
   * @returns {string} The api key of the service.
   */
  createService(serviceName: string): Promise<string>

  findServiceByApiKey(apiKey: string): Promise<Service>

  findAllServices(): Promise<Service[]>
}