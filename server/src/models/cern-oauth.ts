import * as request from "request-promise-native";
import { RequestPromiseOptions } from "request-promise-native";

export class CernOauth {

  public static WEB_SERVICE: string = "web";

  private OAUTH_CLIENT_SECRET: string;
  private OAUTH_REDIRECT_URI: string;
  private OAUTH_CLIENT_ID: string;

  constructor(service: string) {
    if (service === CernOauth.WEB_SERVICE) {
      this.OAUTH_CLIENT_SECRET = process.env.OAUTH_CLIENT_SECRET_WEB
      this.OAUTH_REDIRECT_URI = process.env.OAUTH_REDIRECT_URI_WEB
      this.OAUTH_CLIENT_ID = process.env.OAUTH_CLIENT_ID_WEB
    } else {
      this.OAUTH_CLIENT_SECRET = process.env.OAUTH_CLIENT_SECRET_MOBILE
      this.OAUTH_REDIRECT_URI = process.env.OAUTH_REDIRECT_URI_MOBILE
      this.OAUTH_CLIENT_ID = process.env.OAUTH_CLIENT_ID_MOBILE
    }
  }

  async getToken(code): Promise<String> {
    let options: RequestPromiseOptions = {
      body:
        "code=" +
        code +
        "&grant_type=authorization_code&client_secret=" +
        this.OAUTH_CLIENT_SECRET +
        "&redirect_uri=" +
        this.OAUTH_REDIRECT_URI +
        "&client_id=" + this.OAUTH_CLIENT_ID
    };

    let res = await request.post(process.env.OAUTH_TOKEN_URI, options);
    return JSON.parse(res).access_token;
  }

  async getUser(code): Promise<any> {
    let token = await this.getToken(code);
    let requestOptions: RequestPromiseOptions = {
      method: "GET",
      headers: {
        Authorization: "Bearer " + token
      }
    };

    let res = await request(
      "https://oauthresource.web.cern.ch/api/User",
      requestOptions
    );

    return res;
  }
}
