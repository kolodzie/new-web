import { Notification } from "./notification";
import { User } from "./user";

export class Association {

  static Receive = class {

    static link(notification: Notification, users: User[]) {
      notification.users = users
      for (let user of users)
        user.notifications.push(notification);
    }
  }
}