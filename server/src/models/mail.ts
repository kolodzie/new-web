import { Transporter, createTransport } from "nodemailer";

const MAIL_CONFIG = {
  host: 'cernmx.cern.ch',
  port: 25,
  secure: false, // true for 465, false for other ports
}

export class Mail {

  static transporter: Transporter = createTransport(MAIL_CONFIG);

  static sendMail(from: string, to: string, subject: string, text: string) {
    let mailOptions = {
      from: from + '@cern.ch', // sender address
      to, // list of receivers
      subject, // Subject line
      text // plain text body
    };
    Mail.transporter.sendMail(mailOptions, (error, info) => {
      if (error) {
        return console.log(error);
      }
      console.log(`e-mail sent succesfully: ${info}`)
    });
  }
}