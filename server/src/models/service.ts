import { Entity, PrimaryGeneratedColumn, Column, ManyToMany, JoinTable, ManyToOne, OneToMany } from "typeorm";
import { AuthUtils } from "../utils/authUtils";
import { Notification } from "./notification";

@Entity({ name: "Services" })
export class Service {

  @PrimaryGeneratedColumn()
  id: number

  @Column({ unique: true, nullable: false })
  name: string

  @Column({ unique: true, nullable: true })
  apiKey: string

  @OneToMany(type => Notification, notification => notification.from)
  notifications: Notification[];

  constructor(name: string) {
    this.name = name
  }

  generateNewApiKey(): string {
    const apiKey = AuthUtils.hash(JSON.stringify({
      name: this.name,
      timeStamp: new Date().getTime()
    }))

    //We have hash the apiKey before store it in the database
    this.apiKey = AuthUtils.hash(apiKey)

    return apiKey;
  }

}