import { AbstractRule } from "./abstractRule";
import { ChildEntity } from "typeorm";

@ChildEntity("AllByEmailOrPushRule")
export class AllByEmailOrPush extends AbstractRule {
  
  constructor(rule) {
    super(rule)
  }

  getNotificationChannel(): String {
    return this.channel;
  }

}