import { AbstractRule } from "./abstractRule";
import { Notification } from "../notification";
import { Column, ChildEntity } from "typeorm";

@ChildEntity("FromContains")
export class FromContains extends AbstractRule {

    @Column()
    private string: string;

    constructor(rule) {
        super(rule)
        if (rule) {
            this.string = rule.string
        }
    }

    getNotificationChannel(notification: Notification): string {
        if(notification.from.name.includes(this.string))
            return this.channel
    }

}