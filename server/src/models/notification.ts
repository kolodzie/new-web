import { Entity, Column, PrimaryGeneratedColumn, ManyToMany, ManyToOne } from "typeorm";
import { User } from "./user";
import { Service } from "./service";

@Entity({ name: "Notifications" })
export class Notification {
  @PrimaryGeneratedColumn() id: number;

  @ManyToOne(type => Service, service => service.notifications)
  from: Service;

  @Column() target: string;

  @Column() subject: string;

  @Column() body: string;

  @Column({ default: false })
  sent: boolean;

  @Column({ nullable: true })
  sendAt: Date;

  @Column({ nullable: true })
  sentAt: Date;

  @ManyToMany(type => User, user => user.notifications)
  users: User[];

  constructor(notification: any) {
    if (notification) {
      this.id = notification.id;
      this.from = notification.from;
      this.target = notification.target;
      this.subject = notification.subject;
      this.body = notification.body;
      this.sendAt = notification.sendAt;
      this.sentAt = notification.sentAt;
      this.sent = notification.sent;
      this.users = notification.users;
    }
  }
}
