
const cache = [{ key: '1', value: [JSON.stringify({ id: 1, username: "username" })] }]

export class Cache {

  static get(key: string) {
    return new Promise(resolve => {
      const members = cache.find(i => i.key == key)[0].value
      resolve(members.map(m => {
        return JSON.parse(m)
      }
      ))
    })
  }

  static delete(key: string, value: any) {
    if (!cache.find(k => k.key == key)[0].value.some(v => v == value))
      throw new Error("Error when deleting the data.")
    const newvalues = cache.find(k => k.key == key)[0].value.filter(v => v != value)
    cache.find(k => k.key == key)[0].value = newvalues
    return true
  }

  static save(key, value) {
    let textvalue = JSON.stringify(value)
    cache.find(k => k.key == key)[0].value.push(textvalue)
    return true
  }
}