import { Action } from "routing-controllers";
import * as jwt from "jsonwebtoken";
import * as cookieManager from "node-cookie";
import { AuthUtils } from "../utils/authUtils";
import { Authentication } from "../models/authentication";
import { ServicesService } from "../services/services-service";
import { ServiceFactory } from "../services/services-factory";
import { Service } from "../models/service";


export class AuthorizationChecker {

  static apiKeyHeader = 'api-key';

  static check(action: Action, roles: string[]) {
    if (AuthorizationChecker.isActionMadeByService(action) && roles.includes("SERVICE")) {
      return AuthorizationChecker.authenticateService(action);
    }

    if(roles.includes("USER")){
      return AuthorizationChecker.authenticateUser(action)
    }

    return false;
  }

  static isActionMadeByService(action: Action) {
    return action.request.headers[AuthorizationChecker.apiKeyHeader] !== undefined;
  }

  static authenticateUser(action: Action) {
    try {
      const accessToken = AuthorizationChecker.getAccessToken(action.request);
      const fingerPrint = AuthorizationChecker.getFingerPrint(action.request);

      const decoded: any = jwt.verify(accessToken, process.env.JWT_SECRET);
      action.request.userId = decoded.id
      const fingerPrintHash = AuthUtils.hash(fingerPrint);
      AuthorizationChecker.checkFingerPrint(
        decoded.fingerPrint,
        fingerPrintHash
      );
      if (AuthorizationChecker.isRefreshToken(decoded)) {
        return AuthorizationChecker.isTokenInWhiteList(decoded)
      }
      return true;
    } catch (err) {
      return false;
    }
  }

  static async authenticateService(action: Action) {
    const servicesService: ServicesService = ServiceFactory.getServicesService()
    const apiKey = action.request.headers[AuthorizationChecker.apiKeyHeader];
    const service: Service = await servicesService.findServiceByApiKey(apiKey);
    action.request.service = service;
    return await service ? true : false
  }

  static isTokenInWhiteList(tokenPayload): Promise<boolean> {
    return Authentication.isRefreshtokenInWhiteList(tokenPayload)
  }

  static isRefreshToken(tokenPayload): boolean {
    return !tokenPayload.exp
  }

  static checkFingerPrint(fingerPrint1, fingerPrint2) {
    if (fingerPrint1 !== fingerPrint2)
      throw new Error("The fingerprint does not match");
  }

  static getAccessToken(request): string {
    return request.headers["authorization"].replace("Bearer ", "");
  }

  static getFingerPrint(request): string {
    return cookieManager.get(request, "__FINGERPRINT");
  }
}
