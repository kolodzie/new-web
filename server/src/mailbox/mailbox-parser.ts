import * as Imap from "imap";
import { simpleParser } from "mailparser";
import * as Promise from "bluebird";
import { ServiceFactory } from "../services/services-factory";
import { Notification } from "../models/notification";

Promise.longStackTraces();

const notificationsService = ServiceFactory.getNotificationsService();

export function SendInboxEmails() {
  const imapConfig = {
    user: process.env.MAILBOX_ADDRESS,
    password: process.env.MAILBOX_PASSWORD,
    host: "imap.cern.ch",
    port: 993,
    tls: {
      secureProtocol: "TLSv1_method"
    }
  };

  var imap = new Imap(imapConfig);
  Promise.promisifyAll(imap);

  imap.once("ready", () => execute(imap));
  imap.once("error", function(err) {});

  imap.connect();
}

function execute(imap) {
  imap.openBox("INBOX", false, function(err, box) {
    if (err) console.log(err);
    imap.search(["UNSEEN"], function(err, results) {
      if (err) throw err;
      if (!results || !results.length) {
        imap.end();
        return;
      }
      imap.setFlags(results, ["\\Seen"], function(err) {
        if (err) {
          console.log(JSON.stringify(err, null, 2));
        }
      });

      var f = imap.fetch([results], { bodies: "" });
      f.on("message", function(msg, seqno) {
        console.log("Message #%d", seqno);
        var prefix = "(#" + seqno + ") ";
        msg.on("body", function(stream, info) {
          console.log(prefix + "Body");
          simpleParser(stream, (err, mail) => {
            for (let target of extractTargets(mail))
              notificationsService
                .sendNotification(
                  new Notification({
                    target: target,
                    subject: mail.subject,
                    body: mail.text,
                    sentAt: mail.date
                  })
                )
                .then();
          });
        });
      });
      f.once("error", function(err) {
        console.log("Fetch error: " + err);
      });
      f.once("end", function() {
        imap.end();
      });
    });
  });
}

function extractTargets(mail) {
  return mail.to.value.map(to => to.address.replace("@push.", "@"));
}
