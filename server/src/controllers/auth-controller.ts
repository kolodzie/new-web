import {
  JsonController,
  Post,
  BodyParam,
  Res,
  HeaderParam,
  Authorized,
  Req,
  Get
} from "routing-controllers";
import { Response } from "express";
import { ServiceFactory } from "../services/services-factory";
import { AuthService } from "../services/auth-service";
import * as jwt from "jsonwebtoken";

@JsonController()
export class AuthController {
  authService: AuthService = ServiceFactory.getAuthenticationService();

  @Post("/login")
  async login(@BodyParam("code") code: string, @BodyParam("service") service: string, @Res() response: Response) {
    const {
      randomString,
      accessToken,
      refreshToken
    } = await this.authService.authenticateUser(code, service);

    response = this.setCookie("__FINGERPRINT", randomString, response);
    return response.status(201).json({ accessToken, refreshToken });
  }

  @Post("/refresh")
  @Authorized()
  async refreshTokens(
    @HeaderParam("authorization") authorization: string,
    @Res() response: Response
  ) {
    const token = authorization.replace("Bearer ", "");
    const tokenPayload = jwt.decode(token);

    const {
      randomString,
      accessToken,
      refreshToken
    } = await this.authService.refreshTokens(tokenPayload);

    return this.setCookie("__FINGERPRINT", randomString, response)
      .status(201).json({ accessToken, refreshToken });
  }

  @Get("/me")
  @Authorized()
  async getAuthenticatedUser(@Req() request){
    return this.authService.getAuthenticatedUser(request.userId);
  }

  setCookie(name: string, value: string, res: Response) {
    return res.cookie(name, value, {
      httpOnly: true,
      secure: true,
      sameSite: "Strict"
    });
  }
}
