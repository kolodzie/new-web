import {
  Post,
  JsonController,
  BodyParam,
  Authorized,
  QueryParam,
  Req
} from "routing-controllers";
import { ServiceFactory } from "../services/services-factory";
import { RulesService } from "../services/rules-service";
import { AllByEmailOrPush } from "../models/rules/allByEmailOrPush";
import { FromContains } from "../models/rules/FromContains";

@JsonController("/rules")
export class RulesController {
  rulesService: RulesService = ServiceFactory.getRulesService();

  @Post()
  @Authorized("USER")
  createService(@BodyParam("rule") rule, @QueryParam("type") ruleType: String, @Req() req) {
    let concreteRule;

    if (!ruleType) {
      throw Error("You should specify a rule type")
    }

    switch (ruleType) {
      case "allEmailOrPush":
        concreteRule = new AllByEmailOrPush({ ...rule, user: req.userId });
        break;
      case "fromContains":
        concreteRule = new FromContains({ ...rule, user: req.userId })
    }

    if (!concreteRule)
      throw new Error("The rule type does not exist");

    return this.rulesService.createRule(concreteRule)
  }
}