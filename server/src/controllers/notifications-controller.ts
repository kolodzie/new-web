import {
  Get,
  Post,
  JsonController,
  BodyParam,
  Authorized,
  Req
} from "routing-controllers";
import { ServiceFactory } from "../services/services-factory";
import { NotificationsService } from "../services/notifications-service";
import { Notification } from "../models/notification";

@JsonController("/notifications")
export class NotificationsController {
  notificationsService: NotificationsService = ServiceFactory.getNotificationsService();

  @Authorized("USER")
  @Get()
  findAllNotifications(@Req() req) {
    const userId = req.userId;
    return this.notificationsService.findAllNotifications(userId);
  }

  @Authorized("SERVICE")
  @Post()
  sendNotification(@BodyParam("notification") notification: Notification, @Req() req) {
    notification.from = req.service
    return this.notificationsService.sendNotification(notification);    
  }
}
