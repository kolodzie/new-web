import { createConnection, Connection, ConnectionOptions } from "typeorm";
const configFile = require("../../config/config.json");

class DBManager {
  env: string = process.env.NODE_ENV || "development";
  config = configFile[this.env];
}
