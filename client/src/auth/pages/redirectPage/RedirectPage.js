import React, { Component } from "react";
import { Redirect } from "react-router-dom";
import qs from "qs";

import { notificationsRoute } from "notifications/routes";
import * as authRoutes from "auth/routes";

class RedirectPage extends Component {
  componentDidMount = () => {
    const queryParams = qs.parse(this.props.urlQuery.slice(1));
    if (queryParams.code) {
      this.props.login(queryParams.code);
    }
  };

  render() {
    if (this.props.isAuthenticated) {
      return <Redirect to={notificationsRoute.path} />;
    } else {
      return <Redirect to={authRoutes.loginRoute.path} />;
    }
  }
}

export default RedirectPage;
