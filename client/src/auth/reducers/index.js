import { LOGIN, LOGIN_SUCCESS, LOGIN_FAILURE } from 'auth/actions/Login'
import { isAuthenticated } from 'auth/utils/authUtils'
import {
  REFRESH_TOKEN,
  REFRESH_TOKEN_SUCCESS,
  REFRESH_TOKEN_FAILURE
} from 'auth/actions/RefreshToken'

const initialState = {
  loggedIn: isAuthenticated(),
  loginInProgress: false,
  error: null
}

const checkUnauthorizedAction = action => {
  if (
    typeof action.payload === 'object' &&
    action.payload.toString().includes('401 - Unauthorized')
  ) {
    sessionStorage.removeItem('accessToken')
  }
}

/**
 * Reducer function for the austhentication actions
 *
 * @param state Authentication state
 * @param action
 * @returns {{refresh: boolean, loggedIn: boolean, loginInProgress: boolean, errors: {}}}
 */
export default (state = initialState, action) => {
  checkUnauthorizedAction(action)
  switch (action.type) {
    case LOGIN:
    case REFRESH_TOKEN:
      return {
        ...state,
        loginInProgress: true
      }
    case LOGIN_SUCCESS:
    case REFRESH_TOKEN_SUCCESS:
      sessionStorage.setItem('accessToken', action.payload.accessToken)
      sessionStorage.setItem('refreshToken', action.payload.refreshToken)

      return {
        ...state,
        loginInProgress: false,
        errors: {}
      }
    case LOGIN_FAILURE:
    case REFRESH_TOKEN_FAILURE:
      sessionStorage.removeItem('accessToken')
      sessionStorage.removeItem('refreshToken')
      return {
        ...state,
        loginInProgress: false,
        errors: action.payload.response || {
          non_field_errors: action.payload.statusText
        }
      }
    // case LOGOUT_SUCCESS:
    //   return {
    //     ...state,
    //     loggedIn: false,
    //     errors: {}
    //   };
    default:
      return state
  }
}
