import React, { Component } from 'react'
import buildUrl from 'build-url'

/**
 * The idea of this component is to redirect the user to the Oauth authorization URL of your provider.
 */
export class LoginButton extends Component {
  state = {
    redirected: false,
    authorizeUrl: undefined
  };

  /**
   * Builds the authorization url with the given parameters
   * @returns {*}
   */
  buildAuthorizeUrl = () => {
    const config = {
      client_id: process.env.REACT_APP_OAUTH_CLIENT_ID,
      url: process.env.REACT_APP_AUTHORIZATION_URL,
      redirect_url: process.env.REACT_APP_OAUTH_REDIRECT_URL,
      response_type: 'code'
    }
    return buildUrl(config.url, {
      queryParams: {
        client_id: config.client_id,
        response_type: config.response_type,
        redirect_uri: config.redirect_url
      }
    })
  };

  /**
   * Redirects the user to the Oauth authorization URL
   */
  loginUser = () => {
    const authorizeUrl = this.buildAuthorizeUrl()
    this.setState({
      redirected: true,
      authorizeUrl: authorizeUrl
    })
    window.location.href = authorizeUrl
  };

  render () {
    return (
      <button className={'LoginButton'} color={'blue'} onClick={this.loginUser}>
        Login
      </button>
    )
  }
}

export default LoginButton
