import { RSAA } from "redux-api-middleware";
import { withAuth } from "login/utils/authUtils";

export const LOGOUT = "LOGOUT_REQUEST";
export const LOGOUT_SUCCESS = "LOGOUT_SUCCESS";
export const LOGOUT_FAILURE = "LOGOUT_FAILURE";

export const logout = () => ({
  [RSAA]: {
    endpoint: process.env.BASE_URL + "/logout",
    method: "DELETE",
    credentials: "include",
    headers: withAuth({ "Content-Type": "application/json" }),
    types: [LOGOUT, LOGOUT_SUCCESS, LOGOUT_FAILURE]
  }
});
