import { RSAA } from "redux-api-middleware";
import { withRefresh } from "auth/utils/authUtils";

export const REFRESH_TOKEN = "REFRESH_TOKEN";
export const REFRESH_TOKEN_SUCCESS = "REFRESH_TOKEN_SUCCESS";
export const REFRESH_TOKEN_FAILURE = "REFRESH_TOKEN_FAILURE";

/**
 * Action triggered to refresh the access token when it has expired.
 * It requires a refresh token provided by withRefresh, which contains the CSRF
 * refresh token.
 * @returns {{}} A RSAA request
 */
export const refreshAccessToken = () => ({
  [RSAA]: {
    endpoint: process.env.REACT_APP_BASE_URL + "/refresh",
    method: "POST",
    credentials: "include",
    headers: withRefresh({ "Content-Type": "application/json" }),
    types: [REFRESH_TOKEN, REFRESH_TOKEN_SUCCESS, REFRESH_TOKEN_FAILURE]
  }
});
