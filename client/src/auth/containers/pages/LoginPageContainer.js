import { connect } from 'react-redux'
import { isAuthenticated } from 'auth/utils/authUtils'
import { withRouter } from 'react-router-dom'
import LoginPage from 'auth/pages/loginPage/LoginPage'
import { bindActionCreators } from 'redux'
import * as loginActionCreators from 'auth/actions/Login'

function mapStateToProps ({ auth }) {
  return {
    error: auth.error,
    isAuthenticated: isAuthenticated(),
    loginInProgress: auth.loginInProgress
  }
}

function mapDispatchToProps (dispatch) {
  return bindActionCreators(
    {
      ...loginActionCreators
    },
    dispatch
  )
}

export const LoginPageContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginPage)

export default withRouter(LoginPageContainer)
