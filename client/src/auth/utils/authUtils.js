import jwtDecode from 'jwt-decode'

/**
 * Gets the access token csrf from the cookies
 * @returns {*|{}} The cookie value
 */
export function getAccessToken () {
  return sessionStorage.getItem('accessToken')
}

/**
 * Checks if the access token csrf cookie is present (not expired) or not present (expired)
 * @returns {boolean} (true|false)
 */
export function isAccessTokenExpired () {
  if (!getAccessToken()) return true
  return jwtDecode(getAccessToken()).exp < new Date().getTime() / 1000
}

/**
 * Gets the refresh token csrf from the cookies
 * @returns {*|{}} The cookie value
 */
export function getRefreshToken () {
  return sessionStorage.getItem('refreshToken')
}

/**
 * Checks if the user is authenticated on the application. Refresh token must be present.
 * @returns {boolean} (true|false)
 */
export function isAuthenticated () {
  return getRefreshToken() !== null
}

/**
 * Adds a X-CSRF-TOKEN with the access token attribute to the headers
 * @param headers dict with http headers
 * @returns {function(*): {'X-CSRF-TOKEN': (*|{})}} A function that returns a dict with
 * the new headers
 */
export function withAuth (headers = {}) {
  return state => ({
    ...headers,
    Authorization: 'Bearer ' + getAccessToken()
  })
}

/**
 * Adds a X-CSRF-TOKEN with the refresh token attribute to the headers
 * @param headers dict with http headers
 * @returns {function(*): {'X-CSRF-TOKEN': (*|{})}} A function that returns a dict with
 * the new headers
 */
export function withRefresh (headers = {}) {
  return state => ({
    ...headers,
    Authorization: 'Bearer ' + getRefreshToken()
  })
}
