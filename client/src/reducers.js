import { combineReducers } from 'redux'
import NotificationsReducer from 'notifications/reducers/index'
import AuthReducer from 'auth/reducers/index'
import { reducer as formReducer } from 'redux-form'
import { routerReducer } from 'react-router-redux'
import commonReducer from 'common/reducers/index'

export default combineReducers({
    notifications: NotificationsReducer,
    auth: AuthReducer,
    common: commonReducer,

    // Reducers needed by  other dependencies
    form: formReducer,
    router: routerReducer
})
