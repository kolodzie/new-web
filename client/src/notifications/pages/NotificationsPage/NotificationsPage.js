import React, { Component, Fragment } from 'react'
import NotificationsList from 'notifications/containers/NotificationsList'
import PageTitle from 'common/components/PageTitle/PageTitle'
import CreateNotificationButton from 'notifications/containers/CreateNotificationButton'
import './NotificationsPage.css'

class NotificationsPage extends Component {
    render () {
        return (
            <Fragment>
                <PageTitle title="Notifications Page" icon="notifications" />
                <div className="page-content">
                    <NotificationsList />
                    <CreateNotificationButton buttonStyle="fab-bottom-right" />
                </div>
            </Fragment>
        )
    }
}

export default NotificationsPage
