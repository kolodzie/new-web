import { combineReducers } from 'redux'
import newNotification from './NewNotification'
import notificationsList from './NotificationsList'
import NotificationFormReducer from './NotificationFormReducer'

const notificationsReducer = combineReducers({
    newNotification,
    notificationsList,
    newNotificationForm: NotificationFormReducer
})

export default notificationsReducer
