import {
  GET_NOTIFICATIONS,
  GET_NOTIFICATIONS_SUCCESS,
  GET_NOTIFICATIONS_FAILURE
} from "notifications/actions/GetNotifications";

import { CREATE_NOTIFICATION_SUCCESS } from "notifications/actions/CreateNotification";

const INITIAL_STATE = {
  notifications: [],
  error: null,
  loading: false
};

function processGetNotifications(state) {
  return {
    ...state,
    loading: true
  };
}

function processGetNotificationsSuccess(state, notifications) {
  return {
    notifications: notifications,
    error: null,
    loading: false
  };
}

function processGetNotificationsFailure(state, error) {
  return {
    ...state,
    notifications: [],
    error: error,
    loading: false
  };
}

function processCreateNotificationSuccess(state, notification) {
  return {
    ...state,
    notifications: [...state.notifications, notification]
  };
}

export default function(state = INITIAL_STATE, action) {
  let error;
  switch (action.type) {
    case GET_NOTIFICATIONS:
      return processGetNotifications(state);
    case GET_NOTIFICATIONS_SUCCESS:
      return processGetNotificationsSuccess(state, action.payload);
    case GET_NOTIFICATIONS_FAILURE:
      error = action.payload || { message: action.payload.message };
      return processGetNotificationsFailure(state, error);

    case CREATE_NOTIFICATION_SUCCESS:
      return processCreateNotificationSuccess(state, action.payload);

    default:
      return state;
  }
}
