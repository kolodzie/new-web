
import { OPEN_CREATE_NOTIFICATION_FORM, CLOSE_CREATE_NOTIFICATION_FORM } from './../actions/CreateNotificationFromActions'
import { CREATE_NOTIFICATION_SUCCESS } from 'notifications/actions/CreateNotification'
const INITIAL_STATE = {
    open: false
}


export default (state = INITIAL_STATE, action) => {
    switch(action.type) {
    case OPEN_CREATE_NOTIFICATION_FORM:
        return {
            open: true
        }

    case CLOSE_CREATE_NOTIFICATION_FORM:
        return {
            open: false
        }

    case CREATE_NOTIFICATION_SUCCESS: 
        return {
            open:false
        }
        
    default:
        return state
    }
}