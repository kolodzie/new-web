import React, { Component } from 'react'
import { TextField, Button } from '@material-ui/core'
import './CreateNotificationForm.css'

const RenderTextField = ({ name, input, ...custom }) => (
    <TextField
        fullWidth
        id={name}
        {...input}
        {...custom}
        className="text-field"
    />
)

class NotificationForm extends Component {
  state = {
      newNotification: {}
  }
  createNotificationHandler = (event) => {
      event.preventDefault()
      this.props.createNotification(this.state.newNotification)
  };

  handleChange = (event) => {
      const field = event.target.id
      const value = event.target.value
      this.setState(prevstate => ({ newNotification: { ...prevstate.newNotification, [field]: value } }))
  }

  render () {
      return (
          <form onSubmit={this.createNotificationHandler}>
              <RenderTextField
                  name="from"
                  type="Text"
                  label="From"
                  onChange={this.handleChange}
              />
              <RenderTextField
                  name="target"
                  type="Text"
                  label="Target"
                  onChange={this.handleChange}

              />
              <RenderTextField
                  name="level"
                  type="Text"
                  label="Level*"
                  onChange={this.handleChange}

              />
              <RenderTextField
                  name="meta"
                  type="Text"
                  label="Meta"
              />
              <RenderTextField
                  name="subject"
                  type="Text"
                  label="Subject"
                  onChange={this.handleChange}

              />
              <RenderTextField
                  name="body"
                  type="Text"
                  label="Body"
                  onChange={this.handleChange}

              />
              <Button type="submit" variant="contained" color="primary">
          Send
              </Button>
          </form>
      )
  }
}

export default NotificationForm
