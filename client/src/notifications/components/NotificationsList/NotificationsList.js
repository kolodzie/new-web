import React from "react";
import MaterialTable from "common/components/MaterialTable/MaterialTable";

const columns = [
  { id: "subject", numeric: false, disablePadding: true, label: "Subject" },
  { id: "target", numeric: false, disablePadding: false, label: "Target" },
  { id: "sent", numeric: false, disablePadding: false, label: "Sent" },
  { id: "sendAt", numeric: false, disablePadding: false, label: "Send at" },
  { id: "sentAt", numeric: false, disablePadding: false, label: "Sent at" }
];

class NotificationsList extends React.Component {
  componentDidMount() {
    this.props.getNotifications();
  }

  render() {
    const { notifications } = this.props;

    return <MaterialTable data={notifications} columns={columns} />;
  }
}

export default NotificationsList;
