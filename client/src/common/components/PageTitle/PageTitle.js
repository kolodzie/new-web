import React from "react";
import "./PageTitle.css";
import classNames from "classnames";

const PageTitle = ({ title, icon }) => {
  return (
    <div className="page-title-container">
      <i className={classNames("material-icons", "page-title", "page-icon")}>
        {icon}
      </i>
      <h2 className="page-title">{title}</h2>
    </div>
  );
};

export default PageTitle;
