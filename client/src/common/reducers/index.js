import { combineReducers } from 'redux'
import SnackbarReducer from './SnackbarReducer'

const commonReducer = combineReducers({
    snackbars: SnackbarReducer
})

export default commonReducer
