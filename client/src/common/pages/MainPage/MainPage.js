import routes from 'routes'
import React, { Component } from 'react'
import { Redirect } from 'react-router-dom'
import { Route } from 'react-router'
import NotificationsPage from 'notifications/pages/NotificationsPage/NotificationsPage'
import NavBar from 'common/components/SideNav/SideNav'
import classNames from 'classnames'
import PropTypes from 'prop-types'
import { loginRoute } from 'auth/routes/index.js'
import './MainPage.css'
import  Snackbar  from 'common/containers/components/SnackbarContainer.js'


class MainPage extends Component {
    render () {
        const {snackbars, isAuthenticated} = this.props

        if (!isAuthenticated) {
            return <Redirect to={loginRoute.path}/>
        }

        return (
            <div
                className={classNames('flex-container', 'flex-column', 'full-height')}
            >
                <div
                    style={{
                        height: '40px',
                        alignItems: 'center',
                        backgroundColor: 'rgba(30,30,30,0.95)'
                    }}
                >
                    <h1
                        style={{
                            fontSize: '14px',
                            color: 'white',
                            float: 'left',
                            margin: '10px 20px 10px 20px'
                        }}
                    >
            CERN Accelerating science
                    </h1>
                    <h1
                        style={{
                            fontSize: '14px',
                            color: 'white',
                            float: 'right',
                            margin: '10px 20px 10px 20px'
                        }}
                    >
            Signed in as: username
                    </h1>
                </div>
                <div className={classNames('flex-container', 'flex-item')}>
                    <div className="navbar">
                        <NavBar routes={routes} />
                    </div>
                    <div
                        className={classNames(
                            'flex-container',
                            'flex-item',
                            'flex-column',
                            'content'
                        )}
                    >
                        <Route
                            path="/notifications"
                            component={NotificationsPage}
                            key={1}
                        />
                    </div>
                </div>
                {snackbars.map((snackbar,index) => <Snackbar key={index} index={index} {...snackbar}/>)}
            </div>
        )
    }
}

MainPage.propTypes = {
    isAuthenticated: PropTypes.bool.isRequired
}

export default MainPage
