import { connect } from 'react-redux'
import { isAuthenticated } from 'auth/utils/authUtils'

import MainPage from '../../pages/MainPage/MainPage'

const mapStateToProps = (state) => {
    return {
        errors: state.errors,
        isAuthenticated: isAuthenticated(),
        snackbars: state.common.snackbars
    }
}

export default connect(mapStateToProps)(MainPage)
