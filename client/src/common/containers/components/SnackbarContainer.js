import { connect } from 'react-redux'
import { hideSnackbar } from '../../actions/Snackbar'
import Snackbar from 'common/components/snackbar/Snackbar.js'


const mapStateToProps = () => ({})

const mapDispatchToProps = (dispatch) => {
    return {
        handleClose: (index)=> { dispatch(hideSnackbar(index))}
    }

}

export default connect(mapStateToProps,mapDispatchToProps)(Snackbar)