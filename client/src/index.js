import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './App'
import registerServiceWorker from './registerServiceWorker'
import configureStore from './store/ConfigureStore'
import { Provider } from 'react-redux'
import { ConnectedRouter } from 'react-router-redux'
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import createHistory from 'history/createBrowserHistory'

const history = createHistory()
const store = configureStore(history)

const theme = createMuiTheme({
  palette: {
    primary: { main: '#08c' }
  }
})

ReactDOM.render(
  <MuiThemeProvider theme={theme}>
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <App />
      </ConnectedRouter>
    </Provider>
  </MuiThemeProvider>,
  document.getElementById('root')
)
registerServiceWorker()
