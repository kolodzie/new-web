import { isRSAA, apiMiddleware } from 'redux-api-middleware'

import {
    REFRESH_TOKEN_SUCCESS,
    refreshAccessToken
} from './auth/actions/RefreshToken'
import { getRefreshToken, isAccessTokenExpired } from './auth/utils/authUtils'

export function createApiMiddleware () {
    const postponedRSAAs = []

    return ({ dispatch, getState }) => {
        const rsaaMiddleware = apiMiddleware({ dispatch, getState })
        return next => action => {
            const nextCheckPostponed = nextAction => {
                // Run postponed actions after token refresh
                if (nextAction.type === REFRESH_TOKEN_SUCCESS) {
                    console.debug('nextCheckPostponed')
                    console.debug(nextAction.type)
                    next(nextAction)
                    postponedRSAAs.forEach(postponed => {
                        postponedRSAAs.splice(postponedRSAAs.indexOf(postponed), 1)
                        rsaaMiddleware(next)(postponed)
                    })
                } else {
                    next(nextAction)
                }
            }
            if (isRSAA(action)) {
                console.debug('Is RSAA -> True')
                const refreshToken = getRefreshToken()

                if (refreshToken && isAccessTokenExpired()) {
                    console.debug('Access token is expired but we have refresh token')
                    postponedRSAAs.push(action)
                    console.debug('postponed RSAAs: ', postponedRSAAs)
                    if (postponedRSAAs.length > 0) {
                        return rsaaMiddleware(nextCheckPostponed)(refreshAccessToken())
                    } else {
                        return
                    }
                }

                return rsaaMiddleware(next)(action)
            }
            return next(action)
        }
    }
}

export default createApiMiddleware()
