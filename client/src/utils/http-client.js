import axios from "axios";

export default () => {
  return axios.create({
    baseURL: "http://localhost:8080/api"
    // window.location.hostname === "localhost" ||
    // window.location.hostname === "0.0.0.0"
    //   ? "http://localhost:8081/api/"
    //   : process.env.BASE_URL + "/api"
  });
};
