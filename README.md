# Push Notifications Project development notes

---

This project is public. Do not store any password or sensitive information on the source code. Use secrets when needed.

---

Table of Contents:

- [1. Install applications and dependencies](#1-install-applications-and-dependencies)
  - [a. Cloning the git repo](#a-cloning-the-git-repo)
  - [b. Install the programs needed to run the application](#b-install-the-programs-needed-to-run-the-application)
  - [c. Install and configure PostgreSQL locally](<#c-install-and-configure-postgresql-locally-(for-development-purposes)>)
- [2. Running and debugging the system in your computer](#2-running-and-debugging-the-system-in-your-computer)
  - [a. Configure the database environment variables](#a-configure-the-database-environment-variables)
  - [b. Running the application](#b-running-the-application)
  - [c. Debugging](#c-debugging)
  - [d. Running the applications as Docker containers in your computer](#d-running-the-applications-as-docker-containers-in-your-computer)
  - [e. Using a remote DBOD database instead](#e-using-a-remote-dbod-database-instead)
- [3. Administering the database. Database operations.](#3-administering-the-database-database-operations)
  - [a. Using the psql command line client for PostgreSQL](#a-using-the-psql-command-line-client-for-postgresql)
  - [b. Using DBeaver, a graphical DB client](#b-using-dbeaver-a-graphical-db-client)
  - [c. Typical database operations](#c-typical-database-operations)
- [4. OpenShift and deployment](#4-openshift-and-deployment)
  - [a. Openshift project](#a-openshift-project)
  - [b. Publishing changes](#b-publishing-changes)
- [5. How to create a boilerplate project](#5-how-to-create-a-boilerplate-project)
  - [a. Initalize the client project](#a-initalize-the-client-project)
  - [b. Initialize the server project](#b-initialize-the-server-project)
  - [c. Dependencies for the client project](#c-dependencies-for-the-client-project)
  - [d. Dependencies for the server project](#d-dependencies-for-the-server-project)
  - [e. Database initialization](#e-database-initialization)

<br>

## 1. Install applications and dependencies

### a. Cloning the git repo

After installing git if your system does not have it, run for example:

```bash
git clone https://gitlab.cern.ch/push-notifications/new-web.git
```

This will create a `new-web` directory in your current directory. You might want to rename it to `push-notifications-web` for example.

### b. Install the programs needed to run the application

Here we have two options. Running the system using docker or running it using Node.js and React.js manually.

- #### Using Docker

  The easiest way to run the project is to use docker. To install docker go to [Docker website](https://www.docker.com/community-edition#/download) and follow the instructions to install docker in your OS.

  If you are a Linux user you will need to install `Docker Compose`. Follow the instructions for Linux in the [Docker website](https://docs.docker.com/compose/install/#prerequisites).

- #### Using Node.js and React.js manually

  1.  Install Node.js

      The system is running on top of Node.js .

      Install latest Node version (at least 8). Check your version with

      ```bash
      node -v
      ```

      For in example, in Mac OS, after installing [homebrew](https://brew.sh/), you can run:
      `brew install node`.

  1.  Install React.js

      ```bash
      npm install -g create-react-app
      ```

  1.  Install project dependencies (npm packages)

      Go into the git repo directory. Then run:

      ```bash
      cd client
      npm install
      ```

      This will read the file `client/package.json` and install all needed dependencies inside a new `client/node_modules` folder.

      In the same way, for the server project, run from the root folder:

      ```bash
      cd server
      npm install
      ```

### c. Install and configure PostgreSQL locally (for development purposes)

Download and install a PostgreSQL database server directly from <https://www.postgresql.org/download/>. Usually, the installer will create for you the superuser `postgres` who can administer the whole database server.

With the `pgAdmin` tool, or the database client of your choice, perform the following operations, logging in with the `postgres` user:

1.  Create a database "push_dev". Make sure the encoding is UTF8. You can do this via the pgAdmin UI or with the SQL below:
    `CREATE DATABASE push_dev WITH OWNER = postgres ENCODING = 'UTF8' CONNECTION LIMIT = -1;`

1.  Create a new user "pushdev" with some password. You can do this via the pgAdmin UI or with the SQL below:
    `CREATE USER pushdev WITH LOGIN NOSUPERUSER NOCREATEDB NOCREATEROLE INHERIT NOREPLICATION CONNECTION LIMIT -1 PASSWORD 'xxxxxx';`

1.  In the 'push_dev' database, create a new schema 'push' (not push_dev) which will hold the system's tables. Make the 'pushdev' account its owner:
    `CREATE SCHEMA push AUTHORIZATION pushdev;`

The database should now be ready so that the first time that the server application runs, the tables will be created.

<br>

## 2. Running and debugging the system in your computer

### a. Configure the database environment variables

We need to add a file named `ormconfig.env` to the `server` directory whit the value of the environment variables that the server needs to connect to the database.

```bash
TYPEORM_CONNECTION = postgres
TYPEORM_HOST = localhost #Or the remote server
TYPEORM_USERNAME = XXXX
TYPEORM_PASSWORD = XXXX
TYPEORM_DATABASE = push_dev
TYPEORM_PORT = XXXX
TYPEORM_SYNCHRONIZE = true
TYPEORM_LOGGING = true
TYPEORM_ENTITIES = src/models/*.ts
```

### b. Running the application

Here we have two options. Running the system using docker or running it using Node.js and React.js manually.

- #### Using Docker

  You just need to run the following command inside the root directory of the project.

  ```bash
  docker-compose up
  ```

  The server and the client will reload automatically if you change any of the node.js or react.js source code. In most cases, you do not need to stop and start the containers again if you perform a change in the code.

- #### Using Node.js and React.js manually

  1.  Run the server

      Go into the git repo root directory, and the run the client with:

      ```bash
      cd server
      npm start
      ```

      The server app will reload automatically if you change any of the node.js source code. In most cases, you do not need to kill and run the application again if you perform a change in the code.

  1.  Running the client application

      Go into the git repo root directory, and the run the client with:

      ```bash
      cd client
      npm start
      ```

      The client app will reload automatically if you change any of the react.js source code. Again, in most cases, you do not need to kill and run the application again if you perform a change in the code.

### c. Debugging

Check the instructions in [Node.js documentation](https://nodejs.org/api/debugger.html)

### d. Running the applications as Docker containers in your computer

Run image from gitlab-registry:

```bash
docker run --net=host -d gitlab-registry.cern.ch/push-notifications/web
```

Of course, you will previously need to commit changes in git so that the image is rebuilt.

### e. Using a remote DBOD database instead

You can also use a remote PostgreSQL instance, using the databases of the IT Database On Demand (DBOD) service.

For example, to use the current DBOD database which used by the dev website running on Openshit ( https://push-dev.web.cern.ch ), before running the server app, define `DEVELOPMENT_ENV_DATABASE_URL` like this:

```bash
export DEVELOPMENT_ENV_DATABASE_URL=postgresql://username:password@db-ga507.cern.ch:6602/push_dev
```

To set the correct `username` and `password`, you can check them at <https://openshift.cern.ch/console/project/push-dev/browse/config-maps/development-database-connection-string> if you are a member of the Openshift project.

<br>

## 3. Administering the database. Database operations.

### a. Using the psql command line client for PostgreSQL

Connect to the database with `psql`. For your localhost database:

```sql
psql -h localhost -p 5432 -U pushdev -d push_dev
```

Or for the DBOD database:

```sql
psql -h db-ga507.cern.ch -p 6602 -U username -d push_dev
```

(again, check them at <https://openshift.cern.ch/console/project/push-dev/browse/config-maps/development-database-connection-string> for the right username and password)

Some useful tasks with `psql`:

- Describe the database tables with `\dt`
- Quit `psql` with `\q`

### b. Using DBeaver, a graphical DB client

Another option is to use a GUI database client such as DBeaver, in which case the connection can be done with:

- Host : localhost
- Port : 5432
- Database : push_dev
- User : "postgres" or "pushdev"
- Password : the password that you chose

Or for the remote DBOD database:

- Host : db-ga507.cern.ch
- Port : 6602
- Database : push_dev
- User : admin
- Password : (check it in https://openshift.cern.ch/console/project/push-dev/browse/config-maps/development-database-connection-string )

### c. Typical database operations

- Select content with `SELECT * From "Users";` note the quotes and semi-colon.
- Drop table with `DROP Table "Users";`
- Create a new user with `CREATE USER pushdev with LOGIN PASSWORD "password"` (choose some password)
- Changing an account password with `ALTER USER username WITH PASSWORD 'newpassword'`
- Granting privileges:

```bash
ALTER role pushdev CREATEDB;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public to pushdev;
GRANT ALL PRIVILEGES ON DATABASE "push_dev" to pushdev;
```

If running this in the DBOD database, update the database `pg_hba.conf`to include the new user. This is done through the [dbondemand](https://cern.ch/dbod) interface.

<br>

## 4. OpenShift and deployment

### a. Openshift project

The Openshift development project named `push-dev`.

it is available at <https://openshift.cern.ch/console/project/push-dev>

From the command line, with the Openshift command line client `oc`, you can run:

```bash
oc login https://openshift.cern.ch
```

You can install the `oc` client following the instructions [here](https://openshift.cern.ch/console/command-line) . In particular, in Mac OS you can install it using [homebrew](https://brew.sh/), by running:
`brew install openshift-cli`.

### b. Publishing changes

With git, commit your code changes to the repo. This will trigger the CI (Continuous Integration) and build the project's image (takes around 10 min).

Then with the OpenShift cli trigger the images update:

```bash
oc import-image api
oc import-image web
```

<br>

## 5. How to create a boilerplate project

This section details the steps that were initially used to create the `client` and `server` projects.

This is only for documentation purposes. If you clone this git repository, you will already have the project created in this way.

### a. Initalize the client project

After creating a root folder for your project, go inside it and run:

```bash
vue init webpack client
```

This will create a `client` folder with a boilerplate client project.

### b. Initialize the server project

Create a `server` folder at the top level: `mkdir server` .

Then initialize npm:

```bash
cd server
npm init -f
```

This will create the initial version of the file `package.json` .

### c. Dependencies for the client project

Install axios to make webrequest to the backend

```bash
npm install axios
```

Install vuetify, a package that provides UI components for Vue:

```bash
npm install vuetify
```

These commands will also update the file `package.json`, adding these packages to the list of dependencies.

### d. Dependencies for the server project

Install the nodemon and eslint packages. nodemon allows to monitor node files changes during development .

`npm install nodemon eslint`

This should have updated the file `package.json`, adding these 2 packages to the list of dependencies. The next `npm install` commands will do the same.

Inside the `server` folder, edit the file `package.json`, and replace the `"scripts"` part by:

```json
"scripts": {
    "start": "./node_modules/nodemon/bin/nodemon.js src/app.js --exec 'npm run lint && node'",
    "lint": "./node_modules/.bin/eslint **/*.js"
},
```

Initialize the `eslint` package. Still in the `server` folder, run:

```bash
node node_modules/eslint/bin/eslint.js --init
```

Install express dependencies

```bash
npm install express body-parser cors morgan
```

Install PostgreSQL client and Sequelize ORM node.js packages:

```bash
npm install sequelize sequelize-cli pg pg-hstore
```

Install sequelize globally so that you can use it from the command line:

```bash
npm install -g sequelize sequelize-cli
```

Install joi : a package to easily define rules to validate data in the backend

```bash
npm install joi
```

### e. Database initialization

First run inside the `server/src` folder:

```bash
sequelize init
```

Then, to create the models, run:

```bash
sequelize model:generate --name User --attributes username:string,enabled:boolean
sequelize model:generate --name Notification --attributes target:string,subject:string,body:text,sent:boolean,sentAt:date
```

This will create the folders `server/src/config`, `server/src/models`. It will also create a `server/src/migrations` folder which can be deleted in this first phase.
